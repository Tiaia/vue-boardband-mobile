module.exports = {
  devServer: {
    index: "index.html",
    port: 9997,
    //dev-server在服务器启动后打开浏览器
    open: true,
    // 跨域代理
    proxy: {
      //配置跨域
      "/api": {
        // 后端的地址
        target: "http://localhost:9998",
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "^/api": "",
        },
      },
    },
  },
};

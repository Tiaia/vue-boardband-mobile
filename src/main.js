import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "./style/index.scss"
// Vant 组件库
import Vant from 'vant';
// Vant 样式
import 'vant/lib/index.css';
// 自动设置 REM 基准值，根据设备自适应 html 标签字体大小
import 'amfe-flexible'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$axios = axios 
// 注册 Vant 组件
Vue.use(Vant);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// PostCSS 是一个基于 Node.js 运行一段一个处理 CSS的工具
module.exports = {
  plugins: {
    // px 转 rem
    'postcss-pxtorem': {
      rootValue: 37.5,
      propList: ['*'],
    },
  },
};